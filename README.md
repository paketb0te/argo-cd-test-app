# Argo CD Test App

This repo contains basic k8s manifests to deploy a demo app.

[yaml](yaml) contains plain YAML manifests for a deployment and a service.

Future versions could include helm charts, kustomizations, ksonnet apps, ...

## App of Apps

To deploy applications to multiple clusters at once, use the ["App of Apps"-Pattern](https://argo-cd.readthedocs.io/en/stable/operator-manual/declarative-setup/#app-of-apps).

This means you create one argo app which points to a set of argo apps (instead of regular application manifests).

This way, you can specify different target clusters for those argo apps.

`k apply -f app-of-apps/app-of-apps.yml` sets up a "parent" app which in turn creates multiple "child" apps, each of which deploys services/deployments/etc.
